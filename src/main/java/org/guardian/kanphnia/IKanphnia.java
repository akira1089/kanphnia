package org.guardian.kanphnia;

import java.util.ArrayList;

public interface IKanphnia {
 
    public ArrayList<String> getList();
    
    public String getName();
    public int getSize();

    public boolean hasItem (String item);
    
    public String toString();
}
